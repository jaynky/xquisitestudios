---
layout: post
title:  "Introuction to Jtheme"
date:   April 23, 2019
author: Jay
description: An introduction to Jtheme #for meta description
comments: true
excerpt_separator: <!--more-->
---
<img id="viewport" class="col-md-12 lozad" src="/images/kitten.jpg">

Hello!
Welcome to the Jtheme guided tour. 
Strap into your desk chair, pull out your favorite mechanical keyboard, and turn the music up 
because you are getting a fast overview of the whats and hows of Jtheme. (side note, isn't that kitten *adorable*) 

<!--more-->

## Modularity
One of the key design principles I built this site around was being modular.
Wether that was writing a partial for the different sections of the homepage,
to employing JAMstack (don't know what it is? look it up <a href="https://jamstack.org/">https://jamstack.org/</a>).
This means that you spend less time writing code, and more time writing content or adjusting the style. 
You know, the fun stuff. Furthermore, focusing on modularity means that there is only ever one master theme that can be
configured in a practically infinite set of combinations. You are welcome *bows*.


<div class="d-flex justify-content-center">
<img id="viewport" class="col-md-12 lozad" src="/images/sportscar.jpg">
</div>


## *SPEED*
What's faster than a Wordpress site?
Its a trick question. Anything is faster than a Wordpress site.
Streaming a video over a dial up connection would be faster. 
Not Jtheme. 

Built like a sportscar, Jtheme comes with a "few" utilities to maximize speed.
What's that? your images are slowing down you page? No problem, just add the right selectors and 
the lazy loading library will load them only when the user scrolls that far. 
Got render blocking resources? Using partials, each css file renders onto the page within two style tags.
Its a powerful laxative that speeds up the most bloated pages (my apologies for the metaphor. It was to good to pass). 

These are just a few examples of what can be done to make this theme do donuts around our * Ahem * speed challenged competitors. 


## Libraries and plugins
Part of the modularity that was mentioned above is using libraries.

Jtheme not only has basics such as Bootstrap (both the CSS and JS libraries) but also a few additions like an animation library for both CSS and JS, and lazy loading. As if having multiple libraries was not enough, three major Jekyll-plugins have been added to make you life *even* easier. The three plugins consist of jekyll-sitemap (as its name suggests, it automatically generates a sitemap located at /sitemap.xml), an RSS feed generator, and finally an asset optimization plugin that can be configured to turn your assets from slow trash to optimized treasure.


## Inverse Positives
As much as I love this theme I built, it is a work in progress. Yes I know its a cliche, but you never finish a site, much less a theme.
    As such I am dependent on your *constructive* feedback. This site gets better as it evolves, just don't be surprised when you stumble across a bug. 


## configuration

Before I get back to coding, lets talk about the config file.
I kept it simple which means you have more room to add your custom variables.

It contains the basics such as URL (your domain), logo, etc as well as variables for Disqus and analytics. 
Thats about all for now. Good luck and have fun. Remember if you have any tips for improvement or problems
discord me at Jay#3130. If you are old you can email me at Jaynkystudio@gmx.com. 


  